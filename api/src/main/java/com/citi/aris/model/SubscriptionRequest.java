package com.citi.aris.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class SubscriptionRequest {
    private String symbol;
    private List<Rule> rules;
}
