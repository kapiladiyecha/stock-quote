package com.citi.aris.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Rule {
    private String type;
    private String operator;
    private BigDecimal number;
}