package com.citi.aris.model;

import lombok.Data;

@Data
public class Symbol {
    private String currency;
    private String description;
    private String displaySymbol;
    private String figi;
    private String mic;
    private String symbol;
    private String type;
}
