package com.citi.aris.model;

import lombok.Data;

import java.math.BigDecimal;
import java.math.MathContext;

@Data
public class Quote {
    private BigDecimal c; // Current price
    private BigDecimal h; // High price of the day
    private BigDecimal l; // Low price of the day
    private BigDecimal o; // Open price of the day
    private BigDecimal pc; // Previous close price
    private BigDecimal t; // timestamp
    private String symbol;
    private BigDecimal priceVariation;

    public BigDecimal getPriceVariation() {
        return this.c.subtract(this.pc).divide(this.pc, MathContext.DECIMAL32).multiply(BigDecimal.valueOf(100))
                .setScale(2, BigDecimal.ROUND_CEILING);
    }
}
