package com.citi.aris.service;

import com.citi.aris.controller.StockQuoteController;
import com.citi.aris.model.Quote;
import com.citi.aris.model.Rule;
import com.citi.aris.model.SubscriptionRequest;
import com.citi.aris.model.Symbol;
import com.citi.aris.util.ArisConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StockQuoteService {

    private static final Logger log = LoggerFactory.getLogger(StockQuoteController.class);
    private final Map<String, List<SubscriptionRequest>> userSubscriptions = new HashMap<>();

    RestTemplate restTemplate = new RestTemplate();

    @Value("${finhub.symbol.list.url}")
    private String symbolListUrl;

    @Value("${finhub.quote.url}")
    private String quoteUrl;

    final SimpMessagingTemplate template;

    final MockDataService mockDataService;

    public StockQuoteService(SimpMessagingTemplate template, MockDataService mockDataService) {
        this.template = template;
        this.mockDataService = mockDataService;
    }

    public List<Symbol> getSymbolsByExchange(String exchange) {
        List<Symbol> symbols = new ArrayList<>();
        try {
            ResponseEntity<Symbol[]> response = restTemplate.getForEntity(symbolListUrl.replace("{exchange}", exchange), Symbol[].class);
            if (response.getBody() != null) {
                symbols = Arrays.stream(response.getBody()).sorted(Comparator.comparing(Symbol::getSymbol)).collect(Collectors.toList());
            }
        } catch (RestClientException e) {
            log.error("Error during getSymbolsByExchange::{} => {}", exchange, e.getMessage());
            symbols = mockDataService.getSymbolsByExchange().stream().sorted(Comparator.comparing(Symbol::getSymbol)).collect(Collectors.toList());
        }
        return symbols;
    }

    public Quote getQuote(String symbol) {
        Quote quote = null;
        try {
            ResponseEntity<Quote> response = restTemplate.getForEntity(quoteUrl.replace("{symbol}", symbol), Quote.class);
            if (response.getBody() != null) {
                quote = response.getBody();
                quote.setSymbol(symbol);
            }
        } catch (RestClientException e) {
            log.error("Error during getQuote::{} => {}", symbol, e.getMessage());
            quote = mockDataService.getQuote(symbol);
        }
        return quote;
    }

    public void addSubscription(SubscriptionRequest subscriptionRequest, String user) {

        List<SubscriptionRequest> userSubs = userSubscriptions.get(user);

        if (userSubs != null) {
            userSubs.removeIf(u -> u.getSymbol().equals(subscriptionRequest.getSymbol()));
        } else {
            userSubs = new ArrayList<>();
        }

        userSubs.add(subscriptionRequest);

        userSubscriptions.put(user, userSubs);
    }

    public void removeSubscription(String symbol, String user) {

        List<SubscriptionRequest> userSubs = userSubscriptions.get(user);

        if (userSubs != null) {
            userSubs.removeIf(u -> u.getSymbol().equals(symbol));
        }

        userSubscriptions.put(user, userSubs);
    }

    public void sendMessageToUsers() {
        Set<String> symbols = new HashSet<>();
        Map<String, Quote> quoteBySymbol = new HashMap<>();

        this.userSubscriptions.values().forEach(us -> us.forEach(s -> symbols.add(s.getSymbol())));

        if (symbols.size() > 0) {

            // To avoid redundant calls, combined all symbols and sending 1 time per symbol
            // If api accepts multiple, then we can send list of symbols to avoid multiple rest calls
            symbols.parallelStream().forEach(s -> quoteBySymbol.putIfAbsent(s, getQuote(s)));

            this.userSubscriptions.keySet().parallelStream().forEach(username -> {
                this.userSubscriptions.get(username).parallelStream().forEach(s -> {
                    Quote quote = quoteBySymbol.get(s.getSymbol());
                    if (isRuleMatched(s, quote)) {
                        template.convertAndSendToUser(username, ArisConstants.TOPIC, quote);
                    }
                });
            });
        }
    }

    private boolean isRuleMatched(SubscriptionRequest s, Quote quote) {
        boolean ruleMatched = false;
        if (s.getRules() != null && s.getRules().size() > 1) {
            for (Rule rule : s.getRules()) {
                switch (rule.getType()) {
                    case ArisConstants.PERCENTAGE:
                        if (rule.getOperator().equals(ArisConstants.OP_GT) && quote.getPriceVariation().compareTo(rule.getNumber()) > 0) {
                            ruleMatched = true;
                        } else if (rule.getOperator().equals(ArisConstants.OP_LT) && quote.getPriceVariation().compareTo(rule.getNumber()) < 0) {
                            ruleMatched = true;
                        }
                        break;
                    case ArisConstants.PRICE:
                        if (rule.getOperator().equals(ArisConstants.OP_GT) && quote.getC().compareTo(rule.getNumber()) > 0) {
                            ruleMatched = true;
                        } else if (rule.getOperator().equals(ArisConstants.OP_LT) && quote.getC().compareTo(rule.getNumber()) < 0) {
                            ruleMatched = true;
                        }
                        break;
                }
            }
        } else {
            ruleMatched = true;
        }
        return ruleMatched;
    }
}
