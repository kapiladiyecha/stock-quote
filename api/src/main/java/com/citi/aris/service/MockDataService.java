package com.citi.aris.service;

import com.citi.aris.model.Quote;
import com.citi.aris.model.Symbol;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MockDataService {
    private static final Logger log = LoggerFactory.getLogger(MockDataService.class);

    public List<Symbol> getSymbolsByExchange() {
        log.info("Using MockDataService::getSymbolsByExchange");
        List<Symbol> symbols = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("static/symbols.json")) {
            symbols = Arrays.asList(mapper.readValue(in, Symbol[].class));
        } catch (IOException e) {
            log.error("Error during getSymbolsByExchange::mock => {}", e.getMessage());
        }
        return symbols;
    }

    public Quote getQuote(String symbol) {
        log.info("Using MockDataService::getQuote");
        Quote quotes = null;
        ObjectMapper mapper = new ObjectMapper();
        try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("static/quotes.json")) {
            quotes = Arrays.stream(mapper.readValue(in, Quote[].class)).filter(q -> q.getSymbol().equals(symbol)).findFirst().orElse(null);
        } catch (IOException e) {
            log.error("Error during getQuote::mock => {}", e.getMessage());
        }
        return quotes;
    }

}
