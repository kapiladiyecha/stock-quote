package com.citi.aris.util;

public class ArisConstants {
    private ArisConstants() { }  // Prevents instantiation

    public static final String PERCENTAGE = "Percentage";
    public static final String PRICE = "Price";
    public static final String OP_GT = ">";
    public static final String OP_LT = "<";
    public static final String TOPIC = "/topic/stocks";

}
