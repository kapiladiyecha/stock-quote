package com.citi.aris.controller;

import com.citi.aris.model.Quote;
import com.citi.aris.model.SubscriptionRequest;
import com.citi.aris.model.Symbol;
import com.citi.aris.service.StockQuoteService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@CrossOrigin
@RestController
public class StockQuoteController {

    final StockQuoteService quoteService;

    public StockQuoteController(StockQuoteService quoteService) {
        this.quoteService = quoteService;
    }

    @Cacheable("symbols")
    @ResponseBody
    @GetMapping("stock/symbol/{exchange}")
    public List<Symbol> getSymbols(@PathVariable String exchange) {
        return quoteService.getSymbolsByExchange(exchange);
    }

    @ResponseBody
    @GetMapping("quote/{symbol}")
    public Quote getQuote(@PathVariable String symbol) {
        return quoteService.getQuote(symbol);
    }

    @MessageMapping("subscribe")
    public void subscribe(SubscriptionRequest subscriptionRequest, Principal principal) {
        quoteService.addSubscription(subscriptionRequest, principal.getName());
    }

    @MessageMapping("unsubscribe")
    public void unsubscribe(String symbol, Principal principal) {
        quoteService.removeSubscription(symbol, principal.getName());
    }

    @Scheduled(fixedRate = 10000)
    public void sendMessage() {
        quoteService.sendMessageToUsers();
    }
}
