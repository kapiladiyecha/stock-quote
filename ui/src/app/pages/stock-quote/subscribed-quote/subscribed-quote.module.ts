import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubscribedQuoteComponent} from './subscribed-quote.component';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../../../../@fury/shared/material-components.module';
import {FurySharedModule} from '../../../../@fury/fury-shared.module';
import {ListModule} from '../../../../@fury/shared/list/list.module';
import {BreadcrumbsModule} from '../../../../@fury/shared/breadcrumbs/breadcrumbs.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MaterialModule,
        FurySharedModule,

        // Core
        ListModule,
        BreadcrumbsModule
    ],
    declarations: [SubscribedQuoteComponent],
    exports: [
        SubscribedQuoteComponent
    ]
})
export class SubscribedQuoteModule {
}
