import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribedQuoteComponent } from './subscribed-quote.component';

describe('SubscribedQuoteComponent', () => {
  let component: SubscribedQuoteComponent;
  let fixture: ComponentFixture<SubscribedQuoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubscribedQuoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribedQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
