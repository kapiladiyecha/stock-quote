import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {ListColumn} from '../../../../@fury/shared/list/list-column.model';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {filter} from 'rxjs/operators';
import {QuoteModel} from './quote.model';
import {MessageService} from '../../../services/message.service';
import {StockQuoteService} from '../../../services/stock-quote.service';

@Component({
    selector: 'fury-subscribed-quote',
    templateUrl: './subscribed-quote.component.html',
    styleUrls: ['./subscribed-quote.component.scss']
})
export class SubscribedQuoteComponent implements OnInit, AfterViewInit, OnDestroy {

    /**
     * Simulating a service with HTTP that returns Observables
     * You probably want to remove this and do all requests in a service with HTTP
     */
    subject$: ReplaySubject<QuoteModel[]> = new ReplaySubject<QuoteModel[]>(1);
    data$: Observable<QuoteModel[]> = this.subject$.asObservable();
    quotes: QuoteModel[];

    @Input()
    columns: ListColumn[] = [
        {name: 'Stock', property: 'symbol', isModelProperty: false, visible: true, align: 'left'},
        {name: 'Price', property: 'c', isModelProperty: true, visible: true, align: 'right'},
        {name: 'Variation', property: 'priceVariation', isModelProperty: false, visible: true, align: 'right'},
        {name: 'High', property: 'h', isModelProperty: true, visible: true, align: 'right'},
        {name: 'Low', property: 'l', isModelProperty: true, visible: true, align: 'right'},
        {name: 'Prev Closing', property: 'pc', isModelProperty: true, visible: true, align: 'right'},
        {name: 'As Of', property: 't', isModelProperty: false, visible: true, align: 'left'},
        {name: 'Actions', property: 'actions', isModelProperty: false, visible: true, align: 'right'},
    ] as ListColumn[];
    pageSize = 10;
    dataSource: MatTableDataSource<QuoteModel> | null;

    @Input()
    rowData: QuoteModel[];

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    constructor(private messageService: MessageService, private stockService: StockQuoteService) {
    }

    get visibleColumns() {
        return this.columns.filter(column => column.visible).map(column => column.property);
    }

    /**
     * Example on how to get data and pass it to the table - usually you would want a dedicated service with a HTTP request for this
     * We are simulating this request here.
     */
    getData() {
        return this.messageService.quotes$;
    }

    ngOnInit() {
        this.getData().subscribe(quote => {
            this.subject$.next(quote);
        });

        this.dataSource = new MatTableDataSource();

        this.data$.pipe(
            filter(data => !!data)
        ).subscribe((quote) => {
            this.quotes = quote;
            this.dataSource.data = quote;
        });
    }

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    unsubscribeStock(quote: QuoteModel) {
        this.quotes.splice(this.quotes.findIndex((existingQuote) => existingQuote.symbol === quote.symbol), 1);
        this.subject$.next(this.quotes);
        this.messageService.unsubscribe(quote.symbol);
        this.stockService.openSnackBar('Unsubscribed successfully.', 'OK');
    }

    onFilterChange(value) {
        if (!this.dataSource) {
            return;
        }
        value = value.trim();
        value = value.toLowerCase();
        this.dataSource.filter = value;
    }

    ngOnDestroy() {
    }

}
