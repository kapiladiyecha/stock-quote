import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {debounceTime, map, startWith} from 'rxjs/operators';
import {StockQuoteService} from '../../services/stock-quote.service';
import {SymbolModel} from './stock.model';
import {QuoteModel} from './subscribed-quote/quote.model';
import {MessageService} from '../../services/message.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
    selector: 'fury-stock-quote',
    templateUrl: './stock-quote.component.html',
    styleUrls: ['./stock-quote.component.scss']
})
export class StockQuoteComponent implements OnInit, OnDestroy {

    private maxLoad = 20;
    private _gap = 16;
    gap = `${this._gap}px`;
    stockCtrl: FormControl;
    subscriptionForm: FormGroup;
    arrayItems: {
        title: string;
    }[];
    filteredStates: Observable<SymbolModel[]>;

    stocks: SymbolModel[] = [];
    quote: QuoteModel;

    constructor(private stockService: StockQuoteService,
                private _formBuilder: FormBuilder,
                public messageService: MessageService) {
    }

    ngOnInit() {
        this.initForm();
        this.loadStocks();
        this.messageService.connect();
    }

    ngOnDestroy() {
        this.messageService.disconnect();
    }

    private initForm() {
        this.stockCtrl = new FormControl();
        this.subscriptionForm = this._formBuilder.group({
            symbol: this.stockCtrl,
            rules: this._formBuilder.array([])
        });
        this.arrayItems = [];
    }

    get rules() {
        return this.subscriptionForm.get('rules') as FormArray;
    }

    private loadStocks() {
        this.stockService.getSymbols('US').subscribe((response: SymbolModel[]) => {
            this.stocks = response;
            this.filteredStates = of(response.slice(0, this.maxLoad));
            this.registerValueChange();
        });
    }

    private registerValueChange() {
        this.filteredStates = this.stockCtrl.valueChanges
            .pipe(
                debounceTime(500),
                startWith(''),
                map(state => state ? this.filterStates(state) : this.stocks.slice(0, this.maxLoad))
            );
    }

    filterStates(name: string) {
        return this.stocks.filter(state =>
            (state.symbol.toLowerCase().indexOf(name.toLowerCase()) === 0)
            || state.description.toLowerCase().indexOf(name.toLowerCase()) === 0).slice(0, this.maxLoad);
    }

    onStockSelect() {
        this.quote = null;
        this.arrayItems = [];
        this.rules.clear();
        const selectedStock = this.stockCtrl.value;
        if (selectedStock) {
            this.stockService.getQuote(selectedStock)
                .subscribe((response: QuoteModel) => {
                    this.quote = response;
                });
        }
    }

    addRule() {
        this.rules.push(this._formBuilder.group(
            {
                type: this._formBuilder.control(''),
                operator: this._formBuilder.control(''),
                number: this._formBuilder.control(''),
            }
        ));
        this.arrayItems.push({title: ''});
    }

    removeRule(index: number) {
        this.arrayItems.splice(index, 1);
        this.rules.removeAt(index);
    }

    priceVariation(quote: QuoteModel): number {
        return +(Math.round((quote.c - quote.pc) / quote.pc * 100 * 100) / 100).toFixed(2) || 0;
    }

    onSubscribeClick() {
        this.messageService.subscribe(this.subscriptionForm.getRawValue());
        this.stockService.openSnackBar('Subscribed successfully.', 'OK');
    }
}
