import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../../@fury/shared/material-components.module';
import {FurySharedModule} from '../../../@fury/fury-shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HighlightModule} from '../../../@fury/shared/highlightjs/highlight.module';
import {FuryCardModule} from '../../../@fury/shared/card/card.module';
import {BreadcrumbsModule} from '../../../@fury/shared/breadcrumbs/breadcrumbs.module';
import {StockQuoteComponent} from './stock-quote.component';
import {StockQuoteRoutingModule} from './stock-quote-routing.module';
import {SubscribedQuoteModule} from './subscribed-quote/subscribed-quote.module';

@NgModule({
    imports: [
        CommonModule,
        StockQuoteRoutingModule,
        MaterialModule,
        FurySharedModule,
        ReactiveFormsModule,

        // Core
        HighlightModule,
        FuryCardModule,
        BreadcrumbsModule,
        SubscribedQuoteModule
    ],
    declarations: [StockQuoteComponent],
})
export class StockQuoteModule {
}
