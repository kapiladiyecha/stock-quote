import {Injectable} from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {QuoteModel} from '../pages/stock-quote/subscribed-quote/quote.model';
import {Subscriptions} from '../pages/stock-quote/stock.model';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MessageService {

    webSocketEndPoint = 'http://localhost:8081/ws';
    topic = '/user/topic/stocks';
    stompClient: any;
    public quotes: QuoteModel[] = [];
    public notifications: QuoteModel[] = [];

    quotes$ = new Subject<QuoteModel[]>();
    notifications$ = new Subject<QuoteModel[]>();

    constructor() {
        // this.connect();
    }

    connect() {
        const ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const that = this;
        this.stompClient.connect({}, frame => {
            that.stompClient.subscribe(this.topic, (message) => {
                if (message) {
                    this.onMessageReceived(message);
                }
            });
        }, this.errorCallBack.bind(this));
    }

    disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log('Disconnected');
    }

    // on error, schedule a reconnection attempt
    errorCallBack(error) {
        console.log('errorCallBack -> ' + error);
        setTimeout(() => {
            this.connect();
        }, 5000);
    }

    subscribe(rules: Subscriptions[]) {
        this.stompClient.send('/app/subscribe', {}, JSON.stringify(rules));
    }

    unsubscribe(symbol: String) {
        this.stompClient.send('/app/unsubscribe', {}, symbol);
    }

    onMessageReceived(quote) {
        console.log('Message Received from Server :: ' + quote);
        const newQuote: QuoteModel = JSON.parse(quote.body);
        if (this.quotes.findIndex(q => q.symbol === newQuote.symbol) !== -1) {
            this.quotes.splice(this.quotes.findIndex(q => q.symbol === newQuote.symbol), 1);
        }
        this.quotes.unshift(newQuote);
        this.quotes$.next(this.quotes);
        this.notifications.slice(0, 25);
        this.notifications.unshift(newQuote);
        this.notifications$.next(this.notifications);
    }
}
